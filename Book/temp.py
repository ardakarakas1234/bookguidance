from datetime import datetime

import cv2
import imutils
import sys

from pyzbar import pyzbar

found = set()


def scanQR(img, saveFile=None):
    global found
    # Scan for QR codes
    objects = pyzbar.decode(img)

    # Check for exception
    if saveFile is None:
        raise Exception("No folder is specified!")

    # Find Qr Codes
    for obj in objects:
        barcodeData = obj.data.decode("utf-8")
        (x, y, w, h) = obj.rect
        cv2.putText(img, barcodeData, (x, y - 15), cv2.FONT_HERSHEY_COMPLEX, 1, (159, 0, 0), 3)
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 4)
        if barcodeData not in found:
            print(barcodeData)
            filename = "image_" + str(datetime.now().strftime("%d-%m-%Y_%H-%M-%S")) + ".jpg"
            cv2.imwrite(filename, img)
            saveFile.write("{},{}\n".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S"), barcodeData))
            saveFile.flush()
            found.add(barcodeData)

    return img

def main(argv):
	img = cv2.imread("/books/",sys.argv[1])
	csv = open("barcode.csv", "w+")
	img = scanQR(img, saveFile=csv)  # Scan for QR codes
	#cv2.imshow("Image Scanner", imutils.resize(img, width=1280))
	key = cv2.waitKey(0)
	if key == 27:
	    cv2.destroyAllWindows()
