package com.TermProject.Book.repository;




import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.TermProject.Book.model.Comment;

@Repository
public interface CommentAndStarRepository extends JpaRepository<Comment,Long> {

   /* @Query("SELECT c FROM Comment c WHERE c.id = ?1")
    List<Comment> findCommentsById(Long id);*/
}
