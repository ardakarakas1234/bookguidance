package com.TermProject.Book.repository;

import java.util.List;

import javax.persistence.Tuple;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.TermProject.Book.DTO.BookDTO;
import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.BookReg;

public interface BookRegRepository extends JpaRepository<BookReg,Long> {

	
	@Query("SELECT br.customer.customerId,br.book.id,b.description ,b.imgPath ,b.name,b.star, b.genre,br.registeredAt FROM Book b INNER JOIN BookReg br  ON br.customer.customerId =:customerId AND br.book = b.id")
	public List<Tuple> findBooksOfCustomer(@Param("customerId") Integer customerId);
	
}


/**
 * @Query("SELECT new com.TermProject.Book.DTO.bookDTO() FROM Book b INNER JOIN BookReg br  ON br.customer.customerId = ?1 AND br.book = b.id")
 */