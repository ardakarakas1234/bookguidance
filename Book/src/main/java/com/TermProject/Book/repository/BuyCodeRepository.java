package com.TermProject.Book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.BuyCode;


public interface BuyCodeRepository extends JpaRepository<BuyCode, Integer> {

	
	//@Query("SELECT bc FROM BuyCode bc WHERE bc.codeString =:codeString")
	BuyCode findBuyCodeByCodeString(@Param("codeString")String codeString);

}
