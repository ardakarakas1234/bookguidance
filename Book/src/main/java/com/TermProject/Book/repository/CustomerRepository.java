package com.TermProject.Book.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.TermProject.Book.model.Customer;

import java.util.Optional;
@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {


   @Query("SELECT c FROM Customer c WHERE c.tc =?1")
    Optional<Customer> findCustomerByTC(String TC);
    @Query("SELECT c FROM Customer c WHERE c.email =?1")
    Optional<Customer> findCustomerByEmail(String email);
    @Query("SELECT c FROM Customer c WHERE c.telephoneNumber =?1")
    Optional<Customer> findCustomerByTN(String telephoneNumber);

    Customer findCustomerByCustomerId(Integer id);
    
    Customer findByEmail(String email);
}
