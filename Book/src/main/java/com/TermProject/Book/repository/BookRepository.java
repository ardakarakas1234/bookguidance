package com.TermProject.Book.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.TermProject.Book.model.Book;





@Repository
public interface BookRepository extends JpaRepository<Book,Integer> {

	
	public Book findBookById(Integer id);
	public List<Book> findBooksByName(String name );
	
	//@Query("SELECT b FROM Book b WHERE b.name =?1")
	public Book findBookByName(String name);
	

	//@Query("SELECT b.genre FROM Book b")
	@Query("SELECT b.genre FROM Book b INNER JOIN BookReg br  ON br.customer.customerId =:customerId AND br.book = b.id")
	public List<String> getTheMaxCountOfType(@Param("customerId")Integer customerId);
	
	@Query("SELECT b.author FROM Book b INNER JOIN BookReg br  ON br.customer.customerId =:customerId AND br.book = b.id")
	public List<String> getTheMaxCountOfAuthor(@Param("customerId")Integer customerId);
	
	/*@Query("SELECT b FROM Book b WHERE b.genre =?1")*/
		public List<Book> findAllByGenre(String genre);
	
	//public Card findCustomerByTelephoneNumber(String telephoneNumber);
}