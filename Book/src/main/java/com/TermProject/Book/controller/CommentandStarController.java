package com.TermProject.Book.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.TermProject.Book.DTO.CommentDto;
import com.TermProject.Book.model.Comment;
import com.TermProject.Book.services.CommentAndStarService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/commentandstar")
public class CommentandStarController {

    private final CommentAndStarService commentAndStarService;
    @Autowired

    public CommentandStarController(CommentAndStarService commentAndStarService) {
        this.commentAndStarService = commentAndStarService;
    }
   /* @PostMapping(path = "{id}")
    public ResponseEntity<?> getCommentsById(@PathVariable("id") Long id)
    {
        try {
            List<Comment> commentList = commentAndStarService.getCommentsById(id);
            return new ResponseEntity<>(commentList, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }*/
    @PostMapping(path = "/post")
    public ResponseEntity<?> AddComment(HttpServletRequest request, HttpServletResponse response, @RequestBody CommentDto commentDto)
    {
        try {
            Comment comment=commentAndStarService.addComment(commentDto);
            return new ResponseEntity<>(comment, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(path = "/")
    public ResponseEntity<?> getComments()
    {
        try {
            List<Comment> commentList = commentAndStarService.getComments();
            return new ResponseEntity<>(commentList, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }
    
}
