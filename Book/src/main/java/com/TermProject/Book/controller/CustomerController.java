package com.TermProject.Book.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.TermProject.Book.DTO.BookDTO;
import com.TermProject.Book.DTO.BookSearch;
import com.TermProject.Book.DTO.CustomerRegisterDto;
import com.TermProject.Book.model.Barcode;
import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.Comment;
import com.TermProject.Book.model.Customer;
import com.TermProject.Book.services.CustomerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/customer")
public class CustomerController {


    private  CustomerService customerService ;
    @Autowired
    public CustomerController(CustomerService customerService) { this.customerService = customerService;}

    @GetMapping(path = "/")
    public ResponseEntity<?> getCustomer(HttpServletRequest request, HttpServletResponse response)
    {
        try {
            List<Customer> customerList = customerService.getCustomers();

            return new ResponseEntity<>(customerList, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }
    @PostMapping("/register")
    public ResponseEntity<?>RegisterCustomer(HttpServletRequest request, HttpServletResponse response,@RequestBody CustomerRegisterDto customerRegisterDto) {

        try
        {

            String c1 = customerService.RegisterCustomer(customerRegisterDto);
            return new ResponseEntity<>(c1, HttpStatus.OK);
        }
        catch (Exception e) {
         e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }
    @GetMapping(path = "/{id}/comments")
    public ResponseEntity<?> getAllCommentsByCustomer(@PathVariable("id") Integer id)
    {
        try {
            List<Comment> commentList = customerService.getComments(id);
            return new ResponseEntity<>(commentList, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }
    @GetMapping("/getBooks/{id}")
	public List<BookDTO> getBooks(@PathVariable("id") String id) {
		
		/*logger.trace("getCustomers starts");
		
		logger.trace("getCustomers ends");*/
		return customerService.getBooks(id);
		
	}
    @GetMapping(path = "/test")
    public String test()
    {
        return "Succesfull";
    }
    @PostMapping("/getBook/barcode")
	public ResponseEntity<?> getBookById(Barcode barcode) { 
		/*logger.trace("getCustomerById starts");
		
		logger.trace("getCustomerById ends");*/
	 
		try {
            
            return new ResponseEntity<>(customerService.getBookByBarcode(barcode), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
	}
    @GetMapping("/search")
	public List<Book> searchBook(@RequestBody BookSearch bookSearch) {
		
		/*logger.trace("getCustomers starts");
		
		logger.trace("getCustomers ends");*/
		return customerService.searchBook(bookSearch);
		
	}
    
    
    

}

