package com.TermProject.Book.controller;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.BookReg;
import com.TermProject.Book.model.Comment;
import com.TermProject.Book.services.BookService;








@RestController
@RequestMapping(path = "api/v1/book")
public class BookController {
	@Autowired
	BookService bookService;
	
	// private static final Logger logger = LogManager.getLogger(CustomerController.class);
	
	
	
	@PostMapping("/getBook/{id}")
	public Book getBookById(@PathVariable Integer id) { 
		/*logger.trace("getCustomerById starts");
		
		logger.trace("getCustomerById ends");*/
		return bookService.getBookById(id);
		
	}	
	/*@GetMapping("/getBooks")
	public List<Book> getBooks() {
		
		/*logger.trace("getCustomers starts");
		
		logger.trace("getCustomers ends");*/
	/*	return bookService.getBooks();*/
		
	/*}*/
	
	/*@GetMapping("/getBooks")
	public List<Book> filterBooksByName(String name) {
		
		/*logger.trace("getCustomers starts");
		
		logger.trace("getCustomers ends");*/
		/*return bookService.filterBooksbyName(name);
		
	}*/

	@DeleteMapping("/deleteBook/{id}")
	public boolean deleteBookById(@PathVariable Integer id) { 
		
		/*logger.trace("deleteCustomerById starts");
		
		logger.trace("deleteCustomerById ends");*/
		return bookService.deleteBookById(id);
	
		
	}
	/*
	 * 
	 * @RequestMapping(value = "/scanBarcode/{id}", method = RequestMethod.POST,
    produces = MediaType.IMAGE_JPEG_VALUE)
	 * */
	@PostMapping("/scanBarcode/{id}")
	public Book scanBarcode(@PathVariable Integer id,@RequestParam("image") MultipartFile multipartFile) {
		
		/*logger.trace("addCustomer starts");
	
		logger.trace("addCustomer ends");*/
		Book book2 = new Book();
		try {
			book2 =  bookService.scanBarcode(/*image*/ multipartFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return book2;
		
		
	}
	@PostMapping("/buyBook/{id}")
	public Book buyBook(@PathVariable Integer id,@RequestBody String code) {
		
		/*logger.trace("addCustomer starts");
	
		logger.trace("addCustomer ends");*/
		Book book = new Book();
		try {
			book =  bookService.buyBook(id,code);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return book;
		
		
	}
	@PostMapping("/add")
	public Integer addBook(@RequestBody Book book) {
		
		/*logger.trace("addCustomer starts");
	
		logger.trace("addCustomer ends");*/
		return bookService.addBook(book);
		
		
	}
	@GetMapping("/suggestBook/{id}")
	public List<Book> suggestBooks(@PathVariable("id") Integer id)
	{
		return bookService.suggestBook(id);
	}
	@PostMapping("/comments/{id}")
    public List<Comment> getAllCommentsByRestaurant(@PathVariable("id") Long id)
    {
        try {
            List<Comment> commentList = bookService.getComments(id);
            return commentList;/*new ResponseEntity<>(commentList, HttpStatus.OK);*/
        } catch (Exception e) {
            e.printStackTrace();
            return null;/* new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);*/
            
        }
    }
	
}
