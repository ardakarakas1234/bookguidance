package com.TermProject.Book.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.TermProject.Book.DTO.BookDTO;
import com.TermProject.Book.DTO.BookSearch;
import com.TermProject.Book.DTO.CustomerRegisterDto;
import com.TermProject.Book.model.Barcode;
import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.Comment;
import com.TermProject.Book.model.Customer;

public interface CustomerService {

    public List<Customer> getCustomers();

    public String RegisterCustomer(CustomerRegisterDto customerRegisterDto);

    public List<Comment> getComments(Integer id) ;

	public ResponseEntity<Barcode> getBookByBarcode(Barcode barcode);

	

	public List<BookDTO> getBooks(String id);

	public List<Book> searchBook(BookSearch bookSearch);
}
