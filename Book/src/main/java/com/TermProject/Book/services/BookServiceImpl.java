package com.TermProject.Book.services;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

import org.aspectj.apache.bcel.classfile.Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.TermProject.Book.DTO.BookDTO;
import com.TermProject.Book.controller.BookController;
//import com.TermProject.Book.model.Author;
import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.BookReg;
import com.TermProject.Book.model.BuyCode;
import com.TermProject.Book.model.Comment;
import com.TermProject.Book.model.Customer;
import com.TermProject.Book.repository.BookRegRepository;
import com.TermProject.Book.repository.BookRepository;
import com.TermProject.Book.repository.BuyCodeRepository;
import com.TermProject.Book.repository.CommentAndStarRepository;
import com.TermProject.Book.repository.CustomerRepository;


import antlr.StringUtils;






@Service
public class BookServiceImpl implements BookService {

	
	private static final String USER_AGENT = "Mozilla/5.0";
	@Autowired
	BookRepository bookRepository;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	BookRegRepository bookRegRepository;
	
	
	@Autowired
	BuyCodeRepository codeRepository;
	 //Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
	 @Autowired
	    private CommentAndStarRepository commentAndStarRepository;
	static HashMap<String,Integer> typeNumber = new HashMap<String,Integer>();
	
	static HashMap<String,Integer> authorNumber = new HashMap<String,Integer>();
	@Override
	public Book getBookById(Integer id) { 
		// TODO Auto-generated method stub
		//logger.info("getCustomerById starts");
		Book card = bookRepository.findBookById(id);
		//logger.info("getCustomerById ends");
		return card;
	}
	 
	@Override
	public List<Book> getBooks() {
		// TODO Auto-generated method stub
	//	logger.info("getCustomers starts");
		//logger.info("getCustomers ends");
		return bookRepository.findAll();
	}
	@Override
	public List<Book> filterBooksbyName(String name) {
		// TODO Auto-generated method stub
	//	logger.info("getCustomers starts");
		//logger.info("getCustomers ends");
		List<Book> bookList = bookRepository.findBooksByName(name);
		return bookList;
	}
	@Override
	public boolean deleteBookById(Integer id) {
		// TODO Auto-generated method stub
		//logger.info("deleteCustomerById starts");
		Book card = bookRepository.findBookById(id);
		if(card == null)
		{
			return false;
		}
		bookRepository.deleteById(id);
		Book BookTemp = bookRepository.findBookById(id);
		 if(card != null)
		 {
			//logger.info("deleteCustomerById ends");
			 return true; 
		 }
		// logger.info("deleteCustomerById ends");*/
		return false;
	}

	@Override
	public Integer addBook(Book book) {
		// TODO Auto-generated method stub
		//logger.info("addCustomer starts");
		if(bookRepository.findBookByName(book.getName())!=null)
		{
			//logger.info("addCustomer ends");
			return -1;
		}
		/*if(cardRepository.findCustomerByTelephoneNumber(card.getTelephoneNumber())!=null)
		{
			//logger.info("addCustomer ends");
			return (long) -1;
		}*/
		bookRepository.save(book);
		//logger.info("addCustomer ends");
		return book.getId();
	}

	@Override
	public boolean deleteBook(Book card) {
		// TODO Auto-generated method stub
		//logger.info("deleteCustomer starts");
		Book cardTemp  = bookRepository.findBookById(card.getId());
		if(cardTemp != null)
		{
			bookRepository.delete(cardTemp);
		//	logger.info("deleteCustomer ends");
			return true;
		}
		//logger.info("deleteCustomer ends");
		
		return false;
	}

	@Override
	public Book scanBarcode(MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
		
		
		write(file);
		
		ProcessBuilder b = 
				new ProcessBuilder("python","C:\\Users\\ardak\\.spyder-py3\\temp.py","C:\\Users\\ardak\\.spyder-py3\\"+file.getOriginalFilename());/*,"D:\\Desktop\\FinalProject\\Book\\books\\"+*"books\\"+file.getOriginalFilename());*/
		
		
			Process process = b.start();
			
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			BufferedReader s = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			
			String barcode = null;
			String lines = null;
			while((lines=reader.readLine())!= null)
			{
				System.out.println("lines   "+lines);
				//if(lines.startsWith("Scanned"))
				//{
					barcode = lines;
				//}
			}
			while((lines=s.readLine())!= null)
			{
				System.out.println("error   "+lines);
			}
			
			String resBody = sendURL(barcode);
			String titleOfBook = null;
			String imgPath = null;
			
			String authors = null;
			if(resBody.indexOf("title")!=0)
			{
				int a = resBody.indexOf("title");
				int c = resBody.indexOf(":",a+1);
				int d = resBody.indexOf('"',c+1);
				int e = resBody.indexOf('"',d+1);
				titleOfBook = resBody.substring(d+1,e);
				System.out.println("title:"+titleOfBook );
				if(resBody.indexOf("subtitle")!=0)
				{
					int a1 = resBody.indexOf("subtitle");
					int c1 = resBody.indexOf(":",a1+1);
					int d1 = resBody.indexOf('"',c1+1);
					int e1 = resBody.indexOf('"',d1+1);
					if(!resBody.substring(d1+1,e1).equals("books#volumes"))
					{
						titleOfBook += resBody.substring(d1+1,e1);
					}
					
					System.out.println(titleOfBook );
				}
			}
			if(resBody.indexOf("authors")!=0)
			{
				int a1 = resBody.indexOf("authors");
				int c1 = resBody.indexOf(":",a1+1);
				int d1 = resBody.indexOf('[',c1+1);
				int e1 = resBody.indexOf(']',d1+1);
				
				
				authors = resBody.substring(d1+1,e1);
				System.out.println(authors );
			}
			String desc = null;
			if(resBody.indexOf("description")!=0)
			{
				int a1 = resBody.indexOf("description");
				int c1 = resBody.indexOf(":",a1+1);
				int d1 = resBody.indexOf('"',c1+1);
				int e1 = resBody.indexOf('"',d1+1);
				
				
				desc = resBody.substring(d1+1,e1);
				System.out.println(desc );
			}
			
			if(resBody.indexOf("smallThumbnail")!=0)
			{
				int a1 = resBody.indexOf("smallThumbnail");
				int c1 = resBody.indexOf(":",a1+1);
				int d1 = resBody.indexOf('"',c1+1);
				int e1 = resBody.indexOf('"',d1+1);
				
			imgPath = resBody.substring(d1+1,e1);
				System.out.println(imgPath );
			}
			
			Book bookFind = findBookByName(titleOfBook);
			
			//Customer customer = customerRepository.findCustomerByCustomerId(id);
			Book book = new Book();
			/*if(bookFind!=null && customer!=null)
			{
				BookReg reg = new BookReg();
				reg.setBook(bookFind);
				reg.setCustomer(customer);
				
				reg.setId(bookRegRepository.findAll().size());
				reg.setRegisteredAt(LocalDateTime.now());
				bookRegRepository.save(reg);
				
			}*/
			if(bookFind == null ) { 
				//System.out.println("author list "+ getAuthors(authors));
				
				bookFind = new Book();
				bookFind.setDescription(desc);
				bookFind.setName(titleOfBook);
				bookFind.setStar(2.5f);
				bookFind.setGenre("");
				bookFind.setId(11);
				bookFind.setAuthor(authors);
				bookFind.setImgPath(imgPath);
				
				/*Author author = new Author();
				author.setName("a-a");
				author.setDob(null);
				
				author.setEmail("a-a");
				author.setId(1L);
				author.setSurname("a-a");
				author.setTelephoneNumber("213123123");
				*/
				
				addBook(bookFind);
			}
			
		return bookFind;
	}
	
	
	private Book findBookByName(String titleOfBook) {
		// TODO Auto-generated method stub
		
				
		List<Book> list = bookRepository.findAll();
		for(Book book : list)
		{
			if(book.getName().equals(titleOfBook))
			{
				return book;
			}
		}
		return null;
	}

	private void write(MultipartFile  file) {
		// TODO Auto-generated method stub
		Path fileNameAndPath = Paths.get("C:\\Users\\ardak\\.spyder-py3", file.getOriginalFilename());
		  //fileNames.append(file.getOriginalFilename()+" ");
		  try {
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String sendURL(String ISBN)
	{

		String res = null;
		try {
    		URL obj = new URL("https://www.googleapis.com/books/v1/volumes?q=isbn:"+ISBN);
    		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    		con.setRequestMethod("GET");
    		//con.setRequestProperty("User-Agent", USER_AGENT);
    		int responseCode = con.getResponseCode();
    		System.out.println("GET Response Code :: " + responseCode+"\n"+
    				"url:"+ obj.getFile()
    				);
    		if (responseCode == HttpURLConnection.HTTP_OK) { // success
    			BufferedReader in = new BufferedReader(new InputStreamReader(
    					con.getInputStream()));
    			String inputLine;
    			StringBuffer response = new StringBuffer();

    			while ((inputLine = in.readLine()) != null) {
    				response.append(inputLine);
    			}
    			in.close();

    			// print result
    			System.out.println(response.toString());
    			res = response.toString();
    			
    		} else {
    			System.out.println("GET request not worked");
    		}
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
		return res;
		
	}      
	
/*	public static List<String> getAuthors(String array)
	{
		List<String> authors = new ArrayList<String>();
		int authCounter = 1;
		int a = -1;
		while(true)
		{
			
			a = array.indexOf(",",a+1);
			if(a!=-1)
			{
				authCounter++;
				
			}else {
				break;
			}
		}
		Integer indexStart = -1,indexEnd = -1;
		for(int i = 0 ; i<authCounter;i++)
		{
			indexStart = array.indexOf('"',indexEnd+1);
			indexEnd = array.indexOf('"',indexStart+1);
			String authorName = array.substring(indexStart+1, indexEnd);
			authors.add(authorName);
			
		}
		return authors;
	}*/

	@Override
	public Book buyBook(Integer id,String code) throws IOException {
		// TODO Auto-generated method stubB
		//String c = getData(code);
		List<BuyCode> cList = codeRepository.findAll();
		BuyCode c = null;
		code = code.substring(1, code.length()-1);
		for(BuyCode temp : cList) 
		{
			System.out.println(temp.getCodeString());
			System.out.println(code);
			if(temp.getCodeString().equals(code))
			{
				c=temp;
			}
		}
		
		System.out.println("bookname"+c.getBookName());
		Book book = bookRepository.findBookByName(c.getBookName());
		Customer customer = customerRepository.findCustomerByCustomerId(id);
		BookReg reg = new BookReg();
		
		if(book!=null && customer!=null) 
		{
			
			reg.setBook(book);
			reg.setCustomer(customer);
			
			reg.setId(bookRegRepository.findAll().size());
			reg.setRegisteredAt(LocalDateTime.now());
			bookRegRepository.save(reg);
			
		}
		return book ;
	}

	@Override
	public List<Book> suggestBook(Integer id) {
		// TODO Auto-generated method stub
		//ookRegRepository.find
		List<String> typeList = bookRepository.getTheMaxCountOfType(id); //bütün typeları çek 
		String mostKey;
		List<String> authorList = bookRepository.getTheMaxCountOfAuthor(id);
		for(String s : authorList)
		{
			if(s.contains(","))
			{
				List<String> subKeys = findSubKeys(s);
				addAuthor(subKeys);
			}
			else {
				addAuthor(s); 
			}
		}
		for(String s : typeList)
		{
			if(s.contains(","))
			{
				List<String> subKeys = findSubKeys(s);
				addGenre(subKeys);
			}
			else {
				addGenre(s); 
			}
			
		}
		String type = findMaxType();
		String author = findMaxAuthor();
		
		List<Book> suggestedBook = /*bookRepository.findAllByGenre(type);*/findAllByGenreAndAuthor(type,id,author); //o type a ait olan kitapları çek
		int count = 0;
		List<Book> suggestedBookTopFive = new ArrayList<Book>();
		
		 Random rand = new Random();
		 List<Integer> list = new ArrayList<>();
		 int loop=0;
		while(count<5)
		{
			
			int rand_int1 = rand.nextInt(suggestedBook.size());
			if(list.contains(rand_int1))
			{
				 rand_int1 = rand.nextInt(suggestedBook.size());
				 loop++;
				 if(loop==5)
				 {
					 break;
				 }
			}
			else {
				list.add(rand_int1);
				suggestedBookTopFive.add(suggestedBook.get(rand_int1));
				count++;
			}
			
		}
		return suggestedBookTopFive;
	//	return null;
				
	}
	private void addAuthor(String s) {
		// TODO Auto-generated method stub
		if(authorNumber.containsKey(s))
		{
			authorNumber.put(s, authorNumber.get(s)+1);
		}
		else {
			authorNumber.put(s, 1);
		}
	}

	private void addAuthor(List<String> subKeys) {
		// TODO Auto-generated method stub
		for(String s : authorNumber.keySet())
		{
			if(subKeys.contains(s))
			{
				authorNumber.put(s, authorNumber.get(s)+1);
			}
			else {
				authorNumber.put(s, 1);
			}
		}
		
	}

	private String findMaxAuthor() {
		// TODO Auto-generated method stub
		
		Integer max = 0;
		String author = null;
		
		for(String s : authorNumber.keySet())
		{
			if(max< authorNumber.get(s))
					{
						max= authorNumber.get(s);
						author = s;
					}
		}
		return author;
	}

	private List<Book> findAllByGenreAndAuthor(String type,Integer id,String author) {
		// TODO Auto-generated method stub
		Integer idInt = id.intValue();
		List<Tuple> bookRegList = bookRegRepository.findBooksOfCustomer(idInt);
		List<BookDTO>bookDTOList = bookRegList.stream()
				.map(t -> new BookDTO(t.get(0, Integer.class), t.get(1, Integer.class), t.get(2, String.class), t.get(3, String.class), t.get(4, String.class), t.get(5, Float.class), t.get(6, String.class), t.get(7, LocalDateTime.class)

				)).collect(Collectors.toList());
		List<Integer> bookId = new ArrayList<>();
		for(BookDTO bookDTO : bookDTOList)
		{
			if(bookDTO.getCustomerId()== idInt)
			{
				bookId.add(bookDTO.getBookId());
			}
		}
		List<Book> bookList = bookRepository.findAll();
		
		List<Book> bookListByGenre = new ArrayList<>();
		int count = 0;
		for(Book book : bookList)
		{
			
			for(Integer bookIdInt : bookId)
			{
				if((book.getGenre().equals(type) ||book.getAuthor().equals(author)) && book.getId() == bookIdInt)
				{
					bookListByGenre.add(bookList.get(bookIdInt));
				}
			}
			
		}
		
		return bookListByGenre;
	}

	private String findMaxType() {
		// TODO Auto-generated method stub
		
		/*String[] sArray = {"Literary Fiction","Mystery","Thriller","Horror",
				"Historical","Romance","Western","Bildungsroman","Speculative Fiction",
				"Science Fiction","Fantasy","Dystopian","Magical Realism","Realist  Literature"};*/
		String[] sArray = {"Mystery","Thriller","Horror",
				"Historical","Romance","Fantasy","Dystopian"};
		Integer max = 0;
		String type = null;
		
		for(int i = 0; i< typeNumber.size(); i++)
		{
			if(typeNumber.get(sArray[i])!= null)
			{
				if(max<typeNumber.get(sArray[i]))
				{
					max= typeNumber.get(sArray[i]);
					type = sArray[i];
				}
			}
		}
		return type;
	}

	public  void addGenre(List<String> s)
	{/*
		
		String[] sArray = {"Literary Fiction","Mystery","Thriller","Horror",
				"Historical","Romance","Western","Bildungsroman","Speculative Fiction",
				"Science Fiction","Fantasy","Dystopian","Magical Realism","Realist  Literature"};
		*/
		
		String[] sArray = {"Mystery","Thriller","Horror",
				"Historical","Romance","Fantasy","Dystopian"};
		if(typeNumber.size()!=0)
		{
			typeNumber.clear();
		}
		for(String sKey : s)
		{
			for(String a : sArray)
			{
				if(sKey.equals(a))
				{
					typeNumber.put(sKey, typeNumber.get(sKey)+1);
				}
			}
		}
		
		
		
	}
	public  void addGenre(String s)
	{
		
		/*String[] sArray = {"Literary Fiction","Mystery","Thriller","Horror",
				"Historical","Romance","Western","Bildungsroman","Speculative Fiction",
				"Science Fiction","Fantasy","Dystopian","Magical Realism","Realist  Literature"};
		
		*/
		
		String[] sArray = {"Mystery","Thriller","Horror",
				"Historical","Romance","Fantasy","Dystopian"};
			for(String a : sArray)
			{
				if(s.equals(a))
				{
					if(typeNumber.get(s)== null)
					{
						typeNumber.put(s, 1);
					}
					else {
						typeNumber.put(s, typeNumber.get(s)+1);
					}
				}
				
			}
		
		
		
		
	}
	public static List<String> findSubKeys(String s) {
		// TODO Auto-generated method stub
		List<String> sList = new ArrayList<String>();
		int start = 0,end = 0;
		while(s.contains(","))
		{
			end = s.indexOf(",");
			sList.add(s.substring(start,end));
			s = s.substring(end+1);
			start = 0;
			end = 0;
			
			
		}
		return sList;
	}
	
	public static String getData(String query) {
		
		
		String url = "jdbc:postgresql://localhost:5432/card";
        String user = "postgres";
        String password = "1818";
        String output = null;	
        try (Connection con = DriverManager.getConnection(url, user, password);
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM buy_code b where b.code_string ='"+query+"'")) {

            if (rs.next()) {
                System.out.println(rs.getString(1));
                output =  (String) rs.getObject(1);
            }

        } catch (SQLException ex) {
        
        	return "ERROR while retrieving data: " + ex.getMessage();
        }
        return output;
       /* String output = "";
        try {
            String connectionUrl = ";databaseName=card;user=postgres;password=1818";
            Connection con = null;
            Statement stmt = null;
            ResultSet rs = null;
            Class.forName("org.postgresql.Driver");
                con = DriverManager.getConnection(connectionUrl);
                String SQL = "select * from buy_code b where b.code_string ="+query+"'";
                stmt = con.createStatement();
                rs = stmt.executeQuery(query);
                while (rs.next()) {
                    output =  (String) rs.getObject(1);
                }
                rs.close();
        }
        catch (Exception e) {

            return "ERROR while retrieving data: " + e.getMessage();
        }
        return output;*/
    }

	@Override
	public List<Comment> getComments(Long id) {
		// TODO Auto-generated method stub
		List<Comment> commentList = commentAndStarRepository.findAll();
        List<Comment>  commentListById = new ArrayList<Comment>();
        Integer i = 0;
        Integer idInt = id.intValue();
        while(i<commentList.size())
        {

            if(commentList.get(i).getBook().getId() == idInt)
            {

                commentListById.add(commentList.get(i));
            }
            i++;
        }
        return commentListById;
	}

}



