package com.TermProject.Book.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.TermProject.Book.DTO.BookDTO;
import com.TermProject.Book.DTO.BookSearch;
import com.TermProject.Book.DTO.CustomerRegisterDto;
import com.TermProject.Book.model.Barcode;
import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.BookReg;
import com.TermProject.Book.model.Comment;
import com.TermProject.Book.model.Customer;
import com.TermProject.Book.repository.BookRegRepository;
import com.TermProject.Book.repository.BookRepository;
import com.TermProject.Book.repository.CommentAndStarRepository;
import com.TermProject.Book.repository.CustomerRepository;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerRepository repository;
	@Autowired
	private CommentAndStarRepository commentAndStarRepository;

	@Autowired
	private BookRegRepository bookregRepository;
	PasswordEncoder passwordEncoder;
	
	@Autowired
	private BookRepository bookRepository;
	
	List<Integer> bookId = null;
	public CustomerServiceImpl() {

		this.passwordEncoder = new BCryptPasswordEncoder();
	}

	public List<Customer> getCustomers() {
		return repository.findAll();
	}

	public String RegisterCustomer(CustomerRegisterDto customerRegisterDto) {

		try {
			Customer c1 = new Customer();
			Customer customer = repository.findByEmail(customerRegisterDto.getEmail());
			if (customer != null) {
				System.out.println("Email taken");
				return "Email is already exist";
				// throw new IllegalStateException("email taken");
			}
			boolean b = Pattern.matches(
					"^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$",
					customerRegisterDto.getEmail());
			if (b) {
				String encrypass = this.passwordEncoder.encode(customerRegisterDto.getPassword());
				c1.setPassword(encrypass);
				c1.setDor(LocalDateTime.now());
				c1.setName(customerRegisterDto.getName());
				c1.setTelephoneNumber(customerRegisterDto.getTelephoneNumber());
				c1.setTc(customerRegisterDto.getTC());
				c1.setEmail(customerRegisterDto.getEmail());
				Optional<Customer> CustomerOptional = repository.findCustomerByTC(c1.getTc());

				if (CustomerOptional.isPresent()) {
					throw new IllegalStateException("TC taken");
				}
				Optional<Customer> CustomerOptional2 = repository.findCustomerByEmail(c1.getEmail());
				if (CustomerOptional2.isPresent()) {
					throw new IllegalStateException("Email taken");
				}
				Optional<Customer> CustomerOptional3 = repository.findCustomerByTN(c1.getTelephoneNumber());
				if (CustomerOptional3.isPresent()) {
					throw new IllegalStateException("Telephone Number taken");
				}
				repository.save(c1);
				return "Added successfully";
			} else {
				System.out.println("email is not valid");
				return "email is not valid";
			}

		} catch (IllegalStateException e) {
			System.out.println(e.getMessage());
		}

		return null;
	}

	public List<Comment> getComments(Integer id) {
		List<Comment> commentList = commentAndStarRepository.findAll();
		List<Comment> commentListById = new ArrayList<Comment>();
		Integer i = 0;
		while (i < commentList.size()) {

			if (commentList.get(i).getCustomer().getId() == id) {

				commentListById.add(commentList.get(i));
			}
			i++;
		}
		return commentListById;
	}

	@Override
	public ResponseEntity<Barcode> getBookByBarcode(Barcode barcode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BookDTO> getBooks(String id) {
		// TODO Auto-generated method stub
		List<Object> bookList = null;
		List<BookDTO> bookDTOList = new ArrayList<BookDTO>();
		try {
			if (isNumeric(id)) {
				Integer idInt = Integer.parseInt(id);
				List<Tuple> bookRegTuple =/* null;*/bookregRepository.findBooksOfCustomer(idInt);
				LocalDate local = LocalDate.now();
				bookDTOList = bookRegTuple.stream()
						.map(t -> new BookDTO(t.get(0, Integer.class), t.get(1, Integer.class), t.get(2, String.class), t.get(3, String.class), t.get(4, String.class), t.get(5, Float.class), t.get(6, String.class), t.get(7, LocalDateTime.class)

						)).collect(Collectors.toList());
				/*
				 * for( BookDTO bo : bookDTOList) { System.out.println("book =>" +
				 * bo.getDescription());
				 * 
				 * }
				 */

				return bookDTOList;
			} else {
				throw new Exception();
			}
			/*
			 * t.get(0, Integer.class), t.get(1, Integer.class), t.get(2, String.class),
			 * t.get(3, String.class), t.get(4, String.class), t.get(5, Integer.class),
			 * t.get(6, String.class), t.get(7, LocalDate.class), t.get(8, Integer.class),
			 */

		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("null");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bookDTOList;
	}

	public static boolean isNumeric(String strNum) {
		if (strNum == null) {
			return false;
		}
		try {
			double d = Double.parseDouble(strNum);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	@Override
	public List<Book> searchBook(BookSearch bookSearch) {
		// TODO Auto-generated method stub
		
		List<BookReg> bookRegList = bookregRepository.findAll();
		bookId = new ArrayList<>();
		for(BookReg b : bookRegList)
		{
			if(b.getCustomer().getCustomerId()== bookSearch.getId())
			{
				bookId.add(b.getBook().getId());
			}
			
		}
		List<Book> bookList = bookRepository.findAll();
		List<Book> bookSearchList = new ArrayList<>();
		 bookId = new ArrayList<>();
		for(Book b : bookList)
		{
			if(b.getName().startsWith(bookSearch.getSearchString()) && containsWith(b.getId()))
			{
				bookSearchList.add(b);
			}
			
		}
		
		return bookSearchList;
	}

	private boolean containsWith(Integer id) {
		// TODO Auto-generated method stub
		for(Integer i : bookId)
		{
			if(i==id)
			{
				return true;
			}
			
		}
		return false;
	}
	
}
