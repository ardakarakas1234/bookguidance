package com.TermProject.Book.services;

import com.TermProject.Book.DTO.CommentDto;
import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.Comment;
import com.TermProject.Book.model.Customer;
import com.TermProject.Book.repository.BookRepository;
import com.TermProject.Book.repository.CommentAndStarRepository;
import com.TermProject.Book.repository.CustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentAndStarService {
    @Autowired
    private final CommentAndStarRepository commentRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private BookRepository bookRepository;
    public CommentAndStarService(CommentAndStarRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    /*public List<Comment> getCommentsById(Long id) {
        List<Comment> restaurantList = commentRepository.findCommentsById(id);
        return restaurantList;
    }*/
    public List<Comment> getComments() {
        List<Comment> commentList = commentRepository.findAll();
        return commentList;
    }

    public Comment addComment(CommentDto commentDto) {
        Comment c = new Comment();
        c.setComment_text(commentDto.getComment_text());
        System.out.println("asddd"+commentDto.getComment_text());
        c.setStar(commentDto.getStar());
        c.setIs_validated(true);

        Customer c1 = customerRepository.findByEmail(commentDto.getCustomer().getEmail());
      //  System.out.println("email : " + commentDto.getBook().getEmail());
        Book r1 = findBookByName(commentDto.getBookCommentDTO().getName());
        c.setBook(r1);
        c.setCustomer(c1);
        //c.setRestaurant(commentDto.getRestaurantDto());
        /*c.setCustomer(
                new Customer(
                        2L,
                        "Arda Karakas",
                        "05345222076",
                        "123123124124",
                        "arda.karakas1234@gmail.com",
                        LocalDateTime.of(2011, Month.APRIL,4,5,34,9),
                        "password"

                )
        );
        c.setRestaurant(
                new Restaurant(
                        1L,
                        20L,
                        50L,
                        1345634L,
                        "Tavuk Dunyasi",
                        "arda.karakas1234@gmail.com",
                        LocalDateTime.of(2011, Month.APRIL,4,5,34,9),
                        3,
                        "05345222076",
                        "jshdlakshdhaklsdjasd"

                )
        );
        c.setId(5L);*/
        return commentRepository.save(c);
    }

	private Customer findByEmail(String email) {
		// TODO Auto-generated method stub
		List<Customer> customerList = customerRepository.findAll();
		for(Customer customer : customerList)
		{
			if(customer.getEmail().equals(email))
			{
				return customer;
			}
		}
		return null;
	}

	private Book findBookByName(String name) {
		// TODO Auto-generated method stub
		List<Book> bookList = bookRepository.findAll();
		for(Book book : bookList)
		{
			if(book.getName().equals(name))
			{
				return book;
			}
		}
		return null;
	}
}
