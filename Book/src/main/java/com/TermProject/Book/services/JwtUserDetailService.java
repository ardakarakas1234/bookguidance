package com.TermProject.Book.services;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.TermProject.Book.model.Customer;
import com.TermProject.Book.repository.CustomerRepository;



@Service
public class JwtUserDetailService implements UserDetailsService {

    @Autowired
    private CustomerRepository customerRepository;

    /**
     *
     * @param username(email)
     * @return UserDetails
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Customer user = customerRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with email: " + username);
        }
        return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
    }

}