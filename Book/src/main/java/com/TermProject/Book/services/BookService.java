package com.TermProject.Book.services;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;


import com.TermProject.Book.model.Book;
import com.TermProject.Book.model.BookReg;
import com.TermProject.Book.model.Comment;


public interface BookService {

	
	public Book getBookById(Integer id);
	
	public List<Book> getBooks();
	
	public boolean deleteBookById(Integer id);
	
	public Integer addBook(Book card);
	
	public boolean deleteBook(Book card);
	
	public List<Book> filterBooksbyName(String name);

	public Book scanBarcode(MultipartFile file)throws IOException;

	public Book buyBook(Integer id,String code) throws IOException;

	public List<Book> suggestBook(Integer id);

	public List<Comment> getComments(Long id);
	
}
