package com.TermProject.Book.services;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream.GetField;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class deneme {

	
	public static void main(String[] args) throws IOException {
		
		ProcessBuilder b = 
				new ProcessBuilder("python","C:\\Users\\ardak\\git\\Deneme_Image\\temp.py");
		
		
			Process process = b.start();
			
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			BufferedReader s = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			
			String barcode = null;
			String lines = null;
			while((lines=reader.readLine())!= null)
			{
				System.out.println("lines   "+lines);
				//if(lines.startsWith("Scanned"))
				//{
					barcode = lines;
				//}
			}
			while((lines=s.readLine())!= null)
			{
				System.out.println("error   "+lines);
			}
			
			String resBody = sendURL(barcode);
			String titleOfBook = null;
			String imgPath = null;
			
			String authors = null;
			if(resBody.indexOf("title")!=0)
			{
				int a = resBody.indexOf("title");
				int c = resBody.indexOf(":",a+1);
				int d = resBody.indexOf('"',c+1);
				int e = resBody.indexOf('"',d+1);
				titleOfBook = resBody.substring(d+1,e);
				//System.out.println("title:"+titleOfBook );
				if(resBody.indexOf("subtitle")!=0)
				{
					int a1 = resBody.indexOf("subtitle");
					int c1 = resBody.indexOf(":",a1+1);
					int d1 = resBody.indexOf('"',c1+1);
					int e1 = resBody.indexOf('"',d1+1);
					titleOfBook +=" : "+ resBody.substring(d1+1,e1);
					System.out.println(titleOfBook );
				}
			}
			if(resBody.indexOf("authors")!=0)
			{
				int a1 = resBody.indexOf("authors");
				int c1 = resBody.indexOf(":",a1+1);
				int d1 = resBody.indexOf('[',c1+1);
				int e1 = resBody.indexOf(']',d1+1);
				
				
				authors = resBody.substring(d1+1,e1);
				System.out.println(authors );
			}
			String desc = null;
			if(resBody.indexOf("description")!=0)
			{
				int a1 = resBody.indexOf("description");
				int c1 = resBody.indexOf(":",a1+1);
				int d1 = resBody.indexOf('"',c1+1);
				int e1 = resBody.indexOf('"',d1+1);
				
				
				desc = resBody.substring(d1+1,e1);
				System.out.println(desc );
			}
			
			if(resBody.indexOf("smallThumbnail")!=0)
			{
				int a1 = resBody.indexOf("smallThumbnail");
				int c1 = resBody.indexOf(":",a1+1);
				int d1 = resBody.indexOf('"',c1+1);
				int e1 = resBody.indexOf('"',d1+1);
				
			imgPath = resBody.substring(d1+1,e1);
				System.out.println(imgPath );
			}
			System.out.println("author list "+ getAuthors(authors));
			
	   }

	public static List<String> getAuthors(String array)
	{
		List<String> authors = new ArrayList<String>();
		int authCounter = 1;
		int a = -1;
		while(true)
		{
			
			a = array.indexOf(",",a+1);
			if(a!=-1)
			{
				authCounter++;
				
			}else {
				break;
			}
		}
		Integer indexStart = -1,indexEnd = -1;
		for(int i = 0 ; i<authCounter;i++)
		{
			indexStart = array.indexOf('"',indexEnd+1);
			indexEnd = array.indexOf('"',indexStart+1);
			String authorName = array.substring(indexStart+1, indexEnd);
			authors.add(authorName);
			
		}
		return authors;
	}
	private static final String USER_AGENT = "Mozilla/5.0";
	public static String sendURL(String ISBN)
	{

		String res = null;
		try {
    		URL obj = new URL("https://www.googleapis.com/books/v1/volumes?q=isbn:"+ISBN);
    		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    		con.setRequestMethod("GET");
    		//con.setRequestProperty("User-Agent", USER_AGENT);
    		int responseCode = con.getResponseCode();
    		System.out.println("GET Response Code :: " + responseCode+"\n"+
    				"url:"+ obj.getFile()
    				);
    		if (responseCode == HttpURLConnection.HTTP_OK) { // success
    			BufferedReader in = new BufferedReader(new InputStreamReader(
    					con.getInputStream()));
    			String inputLine;
    			StringBuffer response = new StringBuffer();

    			while ((inputLine = in.readLine()) != null) {
    				response.append(inputLine);
    			}
    			in.close();

    			// print result
    			System.out.println(response.toString());
    			res = response.toString();
    			
    		} else {
    			System.out.println("GET request not worked");
    		}
    	}
    	catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    	}
		return res;
		
	}
}


	