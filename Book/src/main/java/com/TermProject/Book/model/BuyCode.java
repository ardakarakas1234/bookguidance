package com.TermProject.Book.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "buy_code")
public class BuyCode {

	
	@Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
	Integer id;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getCodeString() {
		return codeString;
	}

	public void setCodeString(String codeString) {
		this.codeString = codeString;
	}

	@Column(name = "bookName", length = 255)
	String bookName;
	
	@Column(name = "codeString", length = 255)
	String codeString;
}
