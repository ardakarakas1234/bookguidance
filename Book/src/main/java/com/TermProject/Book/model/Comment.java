package com.TermProject.Book.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Book book;

   @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Customer customer;

   @Column(name = "comment_text")
    private String comment_text;

    @Column(name = "is_validated")
    private boolean is_validated=false;
    
    @Column(name = "star")
    private Long star;


    public Comment(Integer id, Book book, Customer customer, String comment_text, boolean is_validated, Long star) {
        this.id = id;
        this.book = book;
        this.customer = customer;
        this.comment_text = comment_text;
        this.is_validated = is_validated;
        this.star = star;
    }

    public Comment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

   

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public boolean isIs_validated() {
        return is_validated;
    }

    public void setIs_validated(boolean is_validated) {
        this.is_validated = is_validated;
    }

    public Long getStar() {
        return star;
    }

    public void setStar(Long star) {
        this.star = star;
    }

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

   
}

