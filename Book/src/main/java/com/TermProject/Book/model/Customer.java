package com.TermProject.Book.model;


import org.springframework.data.annotation.CreatedBy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )

    @Column(name = "customerId", length = 255)
    private Integer customerId;
    
     @OneToMany(mappedBy = "customer")
     @JsonIgnore
    private List<Comment> commentList;

    @Column(name = "name", length = 255)
    private String name;
     
    @Column(name = "telephone_number", length = 255)
    private String telephoneNumber;
    
    @Column(name = "tc", length = 255)
    private String tc;
    
    @Column(name = "email", length = 255)
    private String email;
    
    @Column(name = "dor", length = 255)
    @CreatedBy
    private LocalDateTime dor;
    
    @Column(name = "password",length = 255)
    private String password;
    
	@OneToMany(mappedBy = "customer")
   private List<BookReg> bookReg = new ArrayList<>();

	public Customer() {
    }

    public Customer(Integer id, String name, String telephoneNumber, String tc, String email, LocalDateTime dor,String password) {

        this.password=password;
        this.customerId = id;
        this.name = name;
        this.telephoneNumber = telephoneNumber;
        this.tc = tc;
        this.email = email;
        this.dor = dor;

    }
    public Customer(List<Comment> commentList, Integer id, String name, String telephoneNumber, String tc, String email, LocalDateTime dor,String password) {
        this.commentList = commentList;
        this.customerId = id;
        this.name = name;
        this.telephoneNumber = telephoneNumber;
        this.tc = tc;
        this.email = email;
        this.dor = dor;
        this.password=password;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public Integer getId() {
        return customerId;
    }

    public void setId(Integer id) {
        this.customerId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTc() {
        return tc;
    }

    public void setTc(String tc) {
        this.tc = tc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getDor() {
        return dor;
    }

    public void setDor(LocalDateTime dor) {
        this.dor = dor;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public List<BookReg> getBookReg() {
		return bookReg;
	}

	public void setBookReg(List<BookReg> bookReg) {
		this.bookReg = bookReg;
	}

  
    
}
