package com.TermProject.Book.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "author")
public class Author {

	 @Id
	    @GeneratedValue(
	            strategy = GenerationType.IDENTITY
	    )

	    @Column(name = "id", length = 255)
	    private Long id;
	     
	     
	     

	    @Column(name = "name", length = 255)
	    private String name;
	    
	    @Column(name = "surname", length = 255)
	    String surname;
	    
	    
	    @Column(name = "telephone_number", length = 255)
	    private String telephoneNumber;
	   
	    @Column(name = "email", length = 255)
	    private String email;
	    @Column(name = "dob", length = 255)
	    @CreatedBy
	    private LocalDateTime dob;
	    
	    
	   /* @OneToMany(mappedBy = "author")
	    private List<AuthorReg> AuthorReg = new ArrayList<>();*/
	    
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getSurname() {
			return surname;
		}
		public void setSurname(String surname) {
			this.surname = surname;
		}
		public String getTelephoneNumber() {
			return telephoneNumber;
		}
		public void setTelephoneNumber(String telephoneNumber) {
			this.telephoneNumber = telephoneNumber;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public LocalDateTime getDob() {
			return dob;
		}
		public void setDob(LocalDateTime dob) {
			this.dob = dob;
		}
	    
	   
	   

}
