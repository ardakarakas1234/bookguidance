package com.TermProject.Book.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@Entity
@Table(name = "book")
public class Book {

	@Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
	Integer id;
	
	@Column(name = "name", length = 255)
	String name;
	
	@Column(name = "description", length = 255)
	String description;
	
	@Column(name = "genre", length = 255)
	String genre;
	
	@Column(name = "star", length = 255)
	Float star;
	
	@Column(name = "imgPath", length = 255)
	String imgPath;
	
	@Column(name = "author", length = 255)
	String author;
	
	 @OneToMany(mappedBy = "book")
	 @JsonIgnore 
	 private List <BookReg> bookReg= new ArrayList<>();
	 
	 @OneToMany(mappedBy = "book")
	    private List<Comment> commentList;
	 
//	 @OneToMany(mappedBy = "book")
//	 private List <AuthorReg> AuthorReg= new ArrayList<>();


	
	
	
	public String getImgPath() {
		return imgPath;
	} 
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	
//	public List<AuthorReg> getAuthorReg() {
//		return AuthorReg;
//	}
//	public void setAuthorReg(List<AuthorReg> authorReg) {
//		AuthorReg = authorReg;
//	}
	public List<BookReg> getBookReg() {
		return bookReg;
	}
	public void setBookReg(List<BookReg> bookReg) {
		this.bookReg = bookReg;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Float getStar() {
		return star;
	}
	public void setStar(Float star) {
		this.star = star;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	
}
