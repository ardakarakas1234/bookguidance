package com.TermProject.Book.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "book_reg")
public class BookReg {

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public LocalDateTime getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(LocalDateTime registeredAt) {
		this.registeredAt = registeredAt;
	}

	@Id
	Integer id;

    @ManyToOne
    @JoinColumn(name = "customer_id"/*, referencedColumnName="customerId"*/) 
    Customer customer;

    @ManyToOne
    @JoinColumn(name = "book_id"/*, referencedColumnName="id"*/)
    Book book;

    LocalDateTime registeredAt; 

    
}
