//package com.TermProject.Book.model;
//
//import javax.persistence.EmbeddedId;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.MapsId;
//
//public class BookRating {
//
//	
//		 @EmbeddedId
//		 BookRatingKey id;
//
//	    @ManyToOne
//	    @MapsId("customerId")
//	    @JoinColumn(name = "customer_id")
//	    Customer customer;
//
//	    @ManyToOne
//	    @MapsId("bookId")
//	    @JoinColumn(name = "book_id")
//	    Book book;
//
//	    int rating;
//
//		public BookRatingKey getId() {
//			return id;
//		}
//
//		public void setId(BookRatingKey id) {
//			this.id = id;
//		}
//
//		public Customer getCustomer() {
//			return customer;
//		}
//
//		public void setCustomer(Customer customer) {
//			this.customer = customer;
//		}
//
//		public Book getBook() {
//			return book;
//		}
//
//		public void setBook(Book book) {
//			this.book = book;
//		}
//
//		public int getRating() {
//			return rating;
//		}
//
//		public void setRating(int rating) {
//			this.rating = rating;
//		}
//	    
//	    
//	    
//}
