package com.TermProject.Book.DTO;



public class CommentDto {

    private String comment_text;
    private Long star;
    private BookCommentDTO bookCommentDTO;
    private CustomerDTO customer;

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public Long getStar() {
        return star;
    }

    public void setStar(Long star) {
        this.star = star;
    }

    

    

	public BookCommentDTO getBookCommentDTO() {
		return bookCommentDTO;
	}

	public void setBookCommentDTO(BookCommentDTO bookCommentDTO) {
		this.bookCommentDTO = bookCommentDTO;
	}

	public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }
}