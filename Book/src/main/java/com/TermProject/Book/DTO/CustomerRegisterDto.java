package com.TermProject.Book.DTO;

import java.time.LocalDate;

public class CustomerRegisterDto {
    private Long id;
    private String name;
    private String telephoneNumber;
    private String TC;
    private String email;
  
    private String password;
    public CustomerRegisterDto() {
    }

    public CustomerRegisterDto(Long id, String name, String telephoneNumber, String TC, String email, String password) {
        this.id = id;
        this.name = name;
        this.telephoneNumber = telephoneNumber;
        this.TC = TC;
        this.email = email;
       
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTC() {
        return TC;
    }

    public void setTC(String TC) {
        this.TC = TC;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
