package com.TermProject.Book.DTO;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Table;


public class BookDTO {

	
	
	Integer customerId;
	
	Integer bookId;
	
	String description;
	
	String imgPath;
	
	String name;
	
	Float star;
	
	String type;
	
	
	LocalDateTime registeredAt;
	
	

	
	
	public BookDTO(Integer customerId, Integer bookId, String description, String imgPath, String name, Float star,
			String type,LocalDateTime registeredAt) {
		super();
		this.customerId = customerId;
		this.bookId = bookId;
		this.description = description;
		this.imgPath = imgPath;
		this.name = name;
		this.star = star;
		this.type = type;
		this.registeredAt = registeredAt;
		
	
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getBookId() {
		return bookId;
	}

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getStar() {
		return star;
	}

	public void setStar(Float star) {
		this.star = star;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LocalDateTime getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(LocalDateTime registeredAt) {
		this.registeredAt = registeredAt;
	}

	

//	public Integer getCustomerId() {
//		return customerId;
//	}
//
//	public void setCustomerId(Integer customerId) {
//		this.customerId = customerId;
//	}
//
//	
//
//	public String getImgPath() {
//		return imgPath;
//	}
//
//	public void setImgPath(String imgPath) {
//		this.imgPath = imgPath;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public Integer getStar() {
//		return star;
//	}
//
//	public void setStar(Integer star) {
//		this.star = star;
//	}
//
//	
//	public String getType() {
//		return type;
//	}
//
//	public void setType(String type) {
//		this.type = type;
//	}
//
//	public LocalDate getRegisteredAt() {
//		return registeredAt;
//	}
//
//	public void setRegisteredAt(LocalDate registeredAt) {
//		this.registeredAt = registeredAt;
//	}
//
//	public Integer getAuthorId() {
//		return authorId;
//	}
//
//	public void setAuthorId(Integer authorId) {
//		this.authorId = authorId;
//	}
//	
	
}
